target := "description.zip"

buildFiles := $(wildcard src/*)
packFiles := $(wildcard dist/*)

.PHONY: all clean install build

all: $(target)
	@touch $(target)

install: all
	cp $(target) ~/Documents/"Hex Kit"/Plugins/

distclean: clean
	-rm -rf node_modules

clean:
	-killall -9 "Hex Kit"
	-rm ~/Documents/"Hex Kit"/Plugins/$(target)
	-rm $(target)

$(target): $(packFiles) build
	npm run package

build: $(buildFiles) node_modules/webpack/package.json
	npm run build

node_modules/webpack/package.json:
	npm install --save-dev webpack
