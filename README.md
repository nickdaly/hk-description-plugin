# Descriptions

This Hex Kit plugin prints out the long and short descriptions on your map for easy printed reference.

Just drop it in your `Documents/hexkit/plugins` or `~/.config/hexkit/plugins` folder and restart Hex Kit.

# TODO

1. Handle description line breaks correctly.

2. Clean up some of the unnecessary implementation details.

# Development

Start with these references:

* https://github.com/rossimo/hexkit-sample

* https://rossimo.github.io/hexkit-sample/

* https://rossimo.github.io/hexkit-sample/modules/_index_.html

* https://rossimo.github.io/hexkit-sample/modules/_types_.html

To build it, try the NPM magic, or run `make`.
