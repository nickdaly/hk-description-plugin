import 'bootstrap/dist/css/bootstrap.css'

import {
    createStore, State, rotateTile, selectTile, Tools,
    listenMouse, TileCoord, setTool, Map, flipTile, Tile, ITileInfo
} from 'hexkit'
import * as React from 'react'
import * as ReactDOM from 'react-dom'
import * as  jsonFormat from 'json-format'
import { Button, Row, Alert, Container, Col } from 'reactstrap'
import { connect, Provider } from 'react-redux'
import { Dispatch } from 'redux'
import * as _ from 'lodash'
import { ipcRenderer as ipc } from 'electron'

const INSPECT_TOOL = Tools.Info

const indexToCoordinate = (index: number, map: Map): TileCoord =>
      index > -1
      ? {
          x: index % map.width,
          y: Math.floor(index / map.width)
      }
      : null

interface In {
    appState: State
}

interface Out {
    startInspecting: () => void
    selectTile: (coordinate: TileCoord) => void
    rotateTile: (coord: TileCoord) => void
    flipTile: (coord: TileCoord) => void
}

let PluginComponent = ({ appState, startInspecting, rotateTile, flipTile }: In & Out) => {
    var rows = [];
    var width = appState.map.width

    for (var i = 0; i < appState.map.infoLayer.length; i++) {
        var tile = appState.map.infoLayer[i];
        var small = tile["label"]["text"]
        var large = tile["data"]
        if (small || large) {
            var x = Math.floor(i%width) + 1
            var y = Math.floor(i/width) + 1

            rows.push(
                <Row>
                <Col style={{ paddingTop: 10 }}>
                    { x + "-" + y + ": " + small }
                </Col></Row>)
        }
        if (large) {
            rows.push(
                <Row>
                    <Col style={{ paddingTop: 10, paddingRight: 10 }}>
                    { large }
                </Col></Row>)
        }
    }

    return <Container fluid={true} style={{ paddingTop: 10, paddingBottom: 10 }}>
        {rows}
    </Container>
}

let Plugin = connect<In, Out>(
    (state: State) => ({
        appState: state
    }),
    (dispatch: Dispatch<State>) => ({
        selectTile: (coordinate: TileCoord) => dispatch(selectTile(coordinate)),
        startInspecting: () => dispatch(setTool(INSPECT_TOOL)),
        rotateTile: (coord: TileCoord) => dispatch(rotateTile(coord)),
        flipTile: (coord: TileCoord) => dispatch(flipTile(coord))
    })
)(PluginComponent)

window.onload = () => {
    let store = createStore()

    ReactDOM.render(
            <Provider store={store}><Plugin /></Provider>,
        document.getElementById('plugin'))
}
